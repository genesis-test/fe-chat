import { mapGetters, mapMutations, mapActions } from "vuex";
import * as authActions from "@/store/auth/actions";

export default {
  computed: {
    ...mapGetters([authActions.AUTH_IS_AUTHENTICATED, authActions.AUTH_TOKEN_GET, authActions.AUTH_USER_GET]),
    auth_isAuth() {
      return this[authActions.AUTH_IS_AUTHENTICATED];
    },
    auth_token() {
      return this[authActions.AUTH_TOKEN_GET]
    },
    auth_user() {
      return this[authActions.AUTH_USER_GET]
    }
  },
  methods: {
    ...mapMutations([
      authActions.AUTH_RESET,
      authActions.AUTH_SET_AUTHENTICATED,
      authActions.AUTH_TOKEN_SET,
      authActions.AUTH_USER_SET
    ]),
    ...mapActions([authActions.AUTH_API_EMAIL_LOGIN, authActions.AUTH_API_EMAIL_SIGNUP]),
    /**
     * API
     * ----------------------------------------------------------------------
     */
    async auth_login(email, password) {
      return await this[authActions.AUTH_API_EMAIL_LOGIN]({ email, password });
    },
    async auth_signUp(email, password, userName) {
      return await this[authActions.AUTH_API_EMAIL_SIGNUP]({ email, password, userName });
    },
    /**
     * Global
     * ----------------------------------------------------------------------
     */
    auth_reset() {
      this[authActions.AUTH_RESET]();
    },
    auth_isAuthSet(val) {
      this[authActions.AUTH_SET_AUTHENTICATED](val);
    },
    auth_tokenSet(val) {
      this[authActions.AUTH_TOKEN_SET](val);
    },
    auth_userSet(val) {
      this[authActions.AUTH_USER_SET](val);
    }
  }
};
