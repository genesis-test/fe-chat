import { mapGetters, mapMutations } from "vuex";
import * as chatActions from "@/store/chat/actions";

import io from 'socket.io-client';

let socket = null;

export default {
  computed: {
    ...mapGetters([
      chatActions.ROOM_USERS_GET,
      chatActions.ROOM_USER_GET,
      chatActions.ROOM_GET,
      chatActions.ROOM_TYPING_GET,
      chatActions.ROOM_MESSAGES_GET,
      chatActions.ROOM_WATERMARK_GET,
      chatActions.ROOM_OFFSET_GET,
      chatActions.ROOM_MORE_GET
    ]),
    room_users() {
      return this[chatActions.ROOM_USERS_GET]
    },
    room_user() {
      return this[chatActions.ROOM_USER_GET]
    },
    room_get() {
      return this[chatActions.ROOM_GET]
    },
    room_typing() {
      return this[chatActions.ROOM_TYPING_GET]
    },
    room_messages() {
      return this[chatActions.ROOM_MESSAGES_GET]
    },
    room_watermark() {
      return this[chatActions.ROOM_WATERMARK_GET]
    },
    room_offset() {
      return this[chatActions.ROOM_OFFSET_GET]
    },
    room_more() {
      return this[chatActions.ROOM_MORE_GET]
    },
  },
  methods: {
    ...mapMutations([
      chatActions.ROOM_USERS_SET,
      chatActions.ROOMS_LAST_MESSAGE,
      chatActions.ROOM_USER_SET,
      chatActions.ROOM_SET,
      chatActions.ROOM_TYPING_SET,
      chatActions.ROOM_MESSAGES_SET,
      chatActions.ROOM_MESSAGE_ADD,
      chatActions.ROOM_MESSAGE_ADD_FIRST,
      chatActions.ROOM_MESSAGE_ADD_LAST_ROOM,
      chatActions.ROOM_MESSAGE_SET,
      chatActions.ROOM_WATERMARK_SET,
      chatActions.ROOM_OFFSET_SET,
      chatActions.ROOM_MORE_SET
    ]),
    /**
     * Chat
     * ----------------------------------------------------------------------
     */
    room_set(val) {
      this[chatActions.ROOM_SET](val)
    },
    room_userSet(val) {
      this[chatActions.ROOM_USER_SET](val)
    },
    room_typingSet(val) {
      this[chatActions.ROOM_TYPING_SET](val)
    },
    room_messagesSet(val) {
      this[chatActions.ROOM_MESSAGES_SET](val)
    },
    room_messageAdd(val) {
      this[chatActions.ROOM_MESSAGE_ADD](val)
    },
    room_messageAddFirst(val) {
      this[chatActions.ROOM_MESSAGE_ADD_FIRST](val)
    },
    room_messageSet(val) {
      this[chatActions.ROOM_MESSAGE_SET](val)
    },
    room_watermarkSet(val) {
      this[chatActions.ROOM_WATERMARK_SET](val)
    },
    room_moreSet(val) {
      this[chatActions.ROOM_MORE_SET](val)
    },
    room_offsetSet(val) {
      this[chatActions.ROOM_OFFSET_SET](val)
    },
    room_userSelect(val) {
      this.room_watermarkSet(null)
      this.room_offsetSet(0)
      this.room_typingSet(false)
      this.room_messagesSet([])
      this.room_userSet(val)
      this.room_moreSet(false)
      this.emit_roomJoin({ targetUserId: val.id })
    },
    room_reset() {
      this.room_typingSet(false);
      this.room_userSet(null)
      this.room_set(null)
      this.room_watermarkSet(null)
      this.room_moreSet(false)
      this.room_offsetSet(0)
      this.room_messagesSet([])
    },
    /**
     * Socket
     * ----------------------------------------------------------------------
     */
    socket_isConnect() {
      return !!socket
    },
    connect() {
      if (!this.auth_isAuth || !this.auth_token) return
      socket = io(process.env.VUE_APP_WEBSOCKET_URI, {
        query: {
          token: this.auth_token
        }
      })
      // System events
      socket.on('connect', this.onConnect);
      socket.on('connect_error', this.onConnectError);
      socket.on('connect_timeout', this.onConnectTimeout);
      socket.on('reconnect', this.onReconnect);
      socket.on('error', this.onError);
      socket.on('disconnect', this.onDisconnect);
      // Custom events
      socket.on(process.env.VUE_APP_WEBSOCKET_BROADCAST_USERS_LOAD, this.onBroadcastUsersLoad);
      socket.on(process.env.VUE_APP_WEBSOCKET_ANSWER_USERS_LOAD, this.onUsersLoad);
      socket.on(process.env.VUE_APP_WEBSOCKET_ANSWER_ROOM_JOIN_ID, this.onRoomJoinId);
      socket.on(process.env.VUE_APP_WEBSOCKET_ANSWER_ROOM_TYPING, this.onRoomTyping);
      socket.on(process.env.VUE_APP_WEBSOCKET_ANSWER_ROOM_MESSAGES_LOAD, this.onRoomMessages);
      socket.on(process.env.VUE_APP_WEBSOCKET_ANSWER_ROOM_MESSAGE_RECEIVE, this.onRoomMessageReceive);
      socket.on(process.env.VUE_APP_WEBSOCKET_ANSWER_ROOM_MESSAGE_UPDATE, this.onRoomMessageUpdate);
    },
    /**
     * System events
     * ----------------------------------------------------------------------
     */
    onConnect() {
      // TODO logic
    },
    onConnectError() {
      // TODO logic
    },
    onConnectTimeout() {
      // TODO logic
    },
    onReconnect() {
      // TODO logic
    },
    onError(errorStr) {
      let errorObj = errorStr
      try {
        errorObj = JSON.parse(errorStr);
      } catch (err) {
        errorObj = errorStr
        console.error(err)
      }
      console.log('onError', errorObj)
    },
    onDisconnect() {
      this.room_reset()
    },
    /**
     * Custom events
     * ----------------------------------------------------------------------
     */
    onBroadcastUsersLoad() {
      this.emit_usersLoad()
    },
    onUsersLoad(dataRes) {
      if (dataRes.status) {
        this[chatActions.ROOM_USERS_SET](dataRes.data)
      }
    },
    onRoomJoinId(dataRes) {
      if (dataRes.status) {
        this.room_set(dataRes.data)
        this.room_offsetSet(0)
        this.room_watermarkSet(Date.now())
        this.emit_messagesLoad()
      }
    },
    onRoomTyping(dataRes) {
      if (dataRes.status) {
        if (this.room_get) {
          if (this.room_get.id === dataRes.data.roomId) {
            this.room_typingSet( dataRes.data.status)
          }
        }
      }
    },
    onRoomMessages(dataRes) {
      if (dataRes.status) {
        if (Array.isArray(dataRes.data.messages)) {
          if (this.room_offset) {
            dataRes.data.messages.reverse().forEach(messageRes => this.room_messageAddFirst(messageRes))
            this.room_moreSet(dataRes.data.more)
            this.$eventHub.$emit(process.env.VUE_APP_EVENT_BUS_MESSAGES_LOADED)
          } else {
            this.room_moreSet(dataRes.data.more)
            dataRes.data.messages.forEach(messageRes => this.room_messageAdd(messageRes))
            this.$eventHub.$emit(process.env.VUE_APP_EVENT_BUS_MESSAGES_SCROLL_BOTTOM)
          }
        }
      }
    },
    onRoomMessageReceive(dataRes) {
      if (dataRes.status) {
        if (this.room_get) {
          if (dataRes.data.roomId === this.room_get.id)
            this.room_messageAdd(dataRes.data)
            this.$eventHub.$emit(process.env.VUE_APP_EVENT_BUS_MESSAGES_SCROLL_BOTTOM)
        }
      }
    },
    onRoomMessageUpdate(dataRes) {
      if (dataRes.status) {
        if (this.room_get) {
          if (dataRes.data.roomId === this.room_get.id) {
            this.room_messageSet(dataRes.data)
          }
        }
      }
    },
    /**
     * Emit
     * ----------------------------------------------------------------------
     */
    emit_roomJoin(data) {
      socket.emit(process.env.VUE_APP_WEBSOCKET_REQUEST_ROOM_JOIN, data);
    },
    emit_usersLoad() {
      socket.emit(process.env.VUE_APP_WEBSOCKET_REQUEST_USERS_LOAD);
    },
    emit_messagesLoad() {
      socket.emit(process.env.VUE_APP_WEBSOCKET_REQUEST_ROOM_MESSAGES_LOAD, {
        roomId: this.room_get.id,
        watermark: this.room_watermark, 
        offset: this.room_offset
      });
    },
    emit_messageTyping(status) {
      socket.emit(process.env.VUE_APP_WEBSOCKET_REQUEST_ROOM_TYPING, {status, roomId: this.room_get.id, targetUserId: this.room_user.id});
    },
    emit_messageSend(message) {
      socket.emit(process.env.VUE_APP_WEBSOCKET_REQUEST_ROOM_MESSAGE_SEND, {message, roomId: this.room_get.id, targetUserId: this.room_user.id});
    },
    emit_messageSeen(messageId) {
      socket.emit(process.env.VUE_APP_WEBSOCKET_REQUEST_ROOM_MESSAGE_SEEN, {messageId, roomId: this.room_get.id, targetUserId: this.room_user.id});
    }
  },
  mounted() {
    if (!this.auth_isAuth || !this.auth_token) return
    if (!this.socket_isConnect()) {
      this.connect()
    }
  }
};
