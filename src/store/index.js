import Vue from 'vue'
import Vuex from 'vuex'

import authStore from "./auth";
import chatStore from "./chat";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  strict: location.hostname === "localhost",
  modules: {
    authStore,
    chatStore
  }
})
