import * as types from "./actions";

const state = {
  users: [],
  roomUser: null,
  room: null,
  typing: false,
  messages: [],
  offset: 0,
  more: false,
  watermark: null
};

const getters = {
  [types.ROOM_USERS_GET]: state => state.users,
  [types.ROOM_USER_GET]: state => state.roomUser,
  [types.ROOM_GET]: state => state.room,
  [types.ROOM_TYPING_GET]: state => state.typing,
  [types.ROOM_MESSAGES_GET]: state => state.messages,
  [types.ROOM_WATERMARK_GET]: state => state.watermark,
  [types.ROOM_OFFSET_GET]: state => state.offset,
  [types.ROOM_MORE_GET]: state => state.more,
};

const mutations = {
  [types.ROOM_USERS_SET]: (state, resp) => (state.users = resp),
  [types.ROOM_USER_SET]: (state, resp) => (state.roomUser = resp),
  [types.ROOM_SET]: (state, resp) => (state.room = resp),
  [types.ROOM_TYPING_SET]: (state, resp) => (state.typing = resp),
  [types.ROOM_WATERMARK_SET]: (state, resp) => (state.watermark = resp),
  [types.ROOM_OFFSET_SET]: (state, resp) => (state.offset = resp),
  [types.ROOM_MORE_SET]: (state, resp) => (state.more = resp),
  [types.ROOM_MESSAGES_SET]: (state, resp) => (state.messages = resp),
  [types.ROOM_MESSAGE_ADD]: (state, resp) => {
    state.messages.push(resp)
    const index = state.users.findIndex(user => user.roomId === resp.roomId)
    if (index === -1) return
    const newUser = Object.assign(state.users[index], {message: resp})
    state.users.splice(index, 1, newUser)
  },
  [types.ROOM_MESSAGE_ADD_FIRST]: (state, resp) => state.messages.unshift(resp),
  [types.ROOM_MESSAGE_SET]: (_, resp) => {
    const index = state.messages.findIndex(item => item.id === resp.messageId)
    if (index === -1) return
    const newMessage = Object.assign(state.messages[index], resp.set)
    state.messages.splice(index, 1, newMessage)
  }
};

const actions = {};

export default {
  state,
  getters,
  actions,
  mutations
};
