import { requestPOST } from "../../utils/fetchRequest";
import * as types from "./actions";

const localToken = localStorage[process.env.VUE_APP_AUTH_FIELD_TOKEN_NAME];

const state = {
  isAuth: false,
  token: localToken ? JSON.parse(localToken) : null,
  user: null
};

const getters = {
  [types.AUTH_IS_AUTHENTICATED]: state => state.isAuth,
  [types.AUTH_TOKEN_GET]: () => state.token,
  [types.AUTH_USER_GET]: () => state.user
};

const mutations = {
  [types.AUTH_RESET]: state => {
    state.isAuth = false;
    state.token = null;
    state.user = null;
    delete localStorage[process.env.VUE_APP_AUTH_FIELD_TOKEN_NAME];
  },
  [types.AUTH_SET_AUTHENTICATED]: (state, data) => (state.isAuth = data),
  [types.AUTH_TOKEN_SET]: (state, data) => {
    if (!data) return;
    localStorage[process.env.VUE_APP_AUTH_FIELD_TOKEN_NAME] = JSON.stringify(data);
    state.token = data;
  },
  [types.AUTH_USER_SET]: (state, data) => state.user = data
};

const actions = {
  [types.AUTH_API_EMAIL_LOGIN]: async ({ commit }, data) => {
    const resultRes = await requestPOST(`/auth/login`, {}, { noUseToken: true, data }, {});
    if (!resultRes.error) {
      commit(types.AUTH_SET_AUTHENTICATED, true);
      commit(types.AUTH_USER_SET, resultRes.data.user);
      commit(types.AUTH_TOKEN_SET, resultRes.data.token);
    }
    return resultRes
  },
  [types.AUTH_API_TOKEN_LOGIN]: async ({ commit }, data) => {
    const resultRes = await requestPOST(`/auth/login/token`, {}, { data }, {});
    if (!resultRes.error) {
      commit(types.AUTH_SET_AUTHENTICATED, true);
      commit(types.AUTH_USER_SET, resultRes.data.user);
    }
    return resultRes
  },
  [types.AUTH_API_EMAIL_SIGNUP]: async ({ commit }, data) => {
    const resultRes = await requestPOST(`/auth/signup`, {}, { noUseToken: true, data }, {})
    if (!resultRes.error) {
      commit(types.AUTH_SET_AUTHENTICATED, true);
      commit(types.AUTH_USER_SET, resultRes.data.user);
      commit(types.AUTH_TOKEN_SET, resultRes.data.token);
    }
    return resultRes
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
