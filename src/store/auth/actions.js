
export const AUTH_IS_AUTHENTICATED = "AUTH_IS_AUTHENTICATED";
export const AUTH_SET_AUTHENTICATED = "AUTH_SET_AUTHENTICATED";
export const AUTH_TOKEN_GET = "AUTH_TOKEN_GET";
export const AUTH_TOKEN_SET = "AUTH_TOKEN_SET";
export const AUTH_RESET = "AUTH_RESET";

export const AUTH_USER_GET = "AUTH_USER_GET";
export const AUTH_USER_SET = "AUTH_USER_SET";

export const AUTH_API_EMAIL_LOGIN = "AUTH_API_EMAIL_LOGIN";
export const AUTH_API_TOKEN_LOGIN = "AUTH_API_TOKEN_LOGIN";
export const AUTH_API_EMAIL_SIGNUP = "AUTH_API_EMAIL_SIGNUP";