import Vue from "vue";
import Router from "vue-router";
import store from "@/store";
import * as authActions from "@/store/auth/actions";
import AuthRouter from "@/components/auth/router";
import ChatRouter from "@/components/chat/router";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    ...AuthRouter,
    ...ChatRouter,
    {
      path: "*",
      name: "notFound",
      meta: { auth: false },
      component: () => import(/* webpackChunkName: "public" */ "@/components/public/NotFoundView.vue")
    }
  ]
});

router.beforeEach(async (to, from, next) => {
  // If not user and rout protect
  if (!store.getters[authActions.AUTH_USER_GET] && to.meta.auth) {
    // If token exists, login with this token and get user or revoke token and go to login
    if (store.getters[authActions.AUTH_TOKEN_GET]) {
      const resultRes = await store.dispatch(authActions.AUTH_API_TOKEN_LOGIN);
      if (resultRes.error) {
        return next({ path: "/login" });
      }
    }
  }
  // Check rout access and go to login if need
  if (to.meta.auth) {
    if (!store.getters[authActions.AUTH_IS_AUTHENTICATED]) {
      return next({ path: "/login" });
    }
  }

  next();
});

export default router;
