import { delay } from "@/utils/delay";
import store from "@/store";
import * as authActions from "@/store/auth/actions";

let serverUri = process.env.VUE_APP_API_URI;

export const fetchUrlGenerate = (url, params) => {
  const urlObj = new URL(`${serverUri}${url}`);
  Object.keys(params).forEach(key => urlObj.searchParams.append(key, params[key]));
  return urlObj;
};

export const fetchObjGenerate = (method, options) => {
  if (!options.headers) {
    options.headers = {};
  }
  const token = store.getters[authActions.AUTH_TOKEN_GET];
  if (!options.noUseToken && token) {
    options.headers["Authorization"] = `Bearer ${token}`;
  }
  const fetchObj = {
    method, // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, cors, *same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, *same-origin, omit
    headers: {
      "Content-Type": "application/json",
      ...options.headers
    },
    redirect: "follow", // manual, *follow, error
    referrer: "no-referrer" // no-referrer, *client
  };
  if (options.data) {
    fetchObj.body = JSON.stringify(options.data); // body data type must match "Content-Type" header
  }
  return fetchObj;
};

export const fetchRequestGenerate = options => {
  return Object.assign(
    {
      retry: 3,
      retryMultiply: 2
    },
    options || {}
  );
};

export const fetchRequest = async (urlObj, fetchObj, requestOpt) => {
  let error = "unknown error in requestCustom"
  for (let i = 0; i < requestOpt.retry; i++) {
    try {
      const resultRes = await fetch(urlObj, fetchObj);
      // Реакция на статус
      if (resultRes.status >= 500) {
        if (i + 1 < requestOpt.retry) {
          await delay(1000 * (i + 1) * requestOpt.retryMultiply);
        }
      } else {
        return await resultRes.json();
      }
    } catch (err) {
      error = err.message;
      if (i + 1 < requestOpt.retry) {
        await delay(1000 * (i + 1) * requestOpt.retryMultiply);
      }
    }
  }
  return {
    local: true,
    error,
    status: false,
    data: null
  };
};

export const requestGET = async (url, params, fetchOpt, options) => {
  const urlObj = fetchUrlGenerate(url, params);
  const fetchObj = fetchObjGenerate("GET", fetchOpt);
  const requestOpt = fetchRequestGenerate(options);
  return await fetchRequest(urlObj, fetchObj, requestOpt);
};

export const requestPOST = async (url, params, fetchOpt, options) => {
  const urlObj = fetchUrlGenerate(url, params);
  const fetchObj = fetchObjGenerate("POST", fetchOpt);
  const requestOpt = fetchRequestGenerate(options);
  return await fetchRequest(urlObj, fetchObj, requestOpt);
};

export const requestPUT = async (url, params, fetchOpt, options) => {
  const urlObj = fetchUrlGenerate(url, params);
  const fetchObj = fetchObjGenerate("PUT", fetchOpt);
  const requestOpt = fetchRequestGenerate(options);
  return await fetchRequest(urlObj, fetchObj, requestOpt);
};

export const requestDELETE = async (url, params, fetchOpt, options) => {
  const urlObj = fetchUrlGenerate(url, params);
  const fetchObj = fetchObjGenerate("DELETE", fetchOpt);
  const requestOpt = fetchRequestGenerate(options);
  return await fetchRequest(urlObj, fetchObj, requestOpt);
};
