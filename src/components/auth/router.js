export default [
  {
    path: "/login",
    name: "login",
    meta: { auth: false },
    component: () => import(/* webpackChunkName: "auth" */ "@/components/auth/LoginView.vue")
  },
  {
    path: "/signup",
    name: "signup",
    meta: { auth: false },
    component: () => import(/* webpackChunkName: "auth" */ "@/components/auth/SignUpView.vue")
  }
];
