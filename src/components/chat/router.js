export default [
  {
    path: "/chat",
    name: "chat",
    meta: { auth: true },
    component: () => import(/* webpackChunkName: "chat" */ "@/components/chat/ChatView.vue")
  }
];
